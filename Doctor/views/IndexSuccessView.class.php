<?php

class Doctor_IndexSuccessView extends SmartyView
{

	/**
	 * Execute any presentation logic and set template attributes.
	 *
	 * @return void
	 *
	 */
	public function execute ()
	{

		// set our template
		$this->setTemplate('IndexSuccess.tpl');

		$params = array();

		foreach($this->getContext()->getRequest()->getAttributeNames() as $name){
			$params[$name] = $this->getContext()->getRequest()->getAttribute($name);
		}
		
		$this->setAttributes($params);
	}

}

?>
