<?php

class Doctor_IDSuccessView extends SmartyView
{

	/**
	 * Execute any presentation logic and set template attributes.
	 *
	 * @return void
	 *
	 */
	public function execute ()
	{
		$context = $this->getContext();
        $controller = $context->getController();
        $request = $context->getRequest();
        $params = $request->getParameters();

        $id = htmlspecialchars($request->getParameter('id'));
        $id_1 = htmlspecialchars($request->getParameter('id_1'));

        var_dump($id);
        var_dump($id_1);

        $request->setAttribute('id', $id);

        $this->setTemplate('IndexSuccess.tpl');

        $params = array();

		foreach($this->getContext()->getRequest()->getAttributeNames() as $name){
			$params[$name] = $this->getContext()->getRequest()->getAttribute($name);
		}
		
		$this->setAttributes($params);
	}

}

?>
